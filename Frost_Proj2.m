% Ryan Frost
% EE30372
% Project 2: ND Feeder 33 Fault
% Due: 10 May, 2021

clear;
clc;

disp('Ryan Frost');
disp('EE30372');
disp('Project 2: ND Feeder 33 Fault');
disp('Due: 10 May, 2021');
disp('----------------- Project 2 Results -----------------');
disp(' ');

%% part (a)
disp('part (a): ');

% positive and negative sequence admittance/impedance matrices provided:
load('seq12mats_33.mat');
Y_pos = Y;
Y_pos_pu = Ypu;
Y_neg = Y;
Y_neg_pu = Ypu;
Z_pos = Z;
Z_pos_pu = Zpu;
Z_neg = Z;
Z_neg_pu = Zpu;

% system base quantities
V_base_ll = 4160;           % 4160 V
V_base_ln = 4160/sqrt(3);   % 2401.78 V
S_base = 1 * 10^6;          % 1 MVA
Z_base = (V_base_ll^2)/(S_base);
I_base = S_base / (sqrt(3) * V_base_ll);

% forming zero-sequence impedance matrices
% relevant impedance parameters
Z_trans_0 = 1j*3;   % zero-sequence impedance for all transformers
Z_N = 3;            % neutral grounded through zero-sequence res. of 3 ohm
Z_g0 = 1j*5;        % generator at bus 1 internal zero-sequence impedance
Z_0 = (3*Z_N + Z_g0);
Z_build = (2 + 1j); % zero-seq. impedance for buildings in diagram
% converting to admittance parameters
Y_0 = inv(Z_0);

% transformer ratios
a = 4160/480;
a2 = 4160/240; % ratio at single-phase transformer (JACC South)

% 5 kV system, Three 1/C Cables, zero sequence
Z_line_500 = 0.338 + 1j*0.281;  % 500 kcmil, ohms/1000ft
Z_line_40 = 0.380 + 1j*0.367;   % 4/0 cable, ohms/1000ft

% bus-to-bus line impedances
Z_line12 = ((155 + 478 + 522)/(1000)) * Z_line_500;
Z_line23 = ((940 + 645 + 260)/(1000)) * Z_line_500;
Z_line34 = ((326)/(1000)) * Z_line_500;
Z_line45 = ((440)/(1000)) * Z_line_40;
Z_line46 = ((638 + 85)/(1000)) * Z_line_500;
Z_line67 = ((680)/(1000)) * Z_line_500;
Z_line78 = ((20 + 157 + 25 + 35 + 346 + 25)/(1000)) * Z_line_500;
Z_line89 = ((((25 + 692 + 15)/(1000)) * Z_line_500) + (((374 + 15))/(1000)) * Z_line_40);
% converting to admittances
Y_line12 = inv(Z_line12);
Y_line23 = inv(Z_line23);
Y_line34 = inv(Z_line34);
Y_line45 = inv(Z_line45);
Y_line46 = inv(Z_line46);
Y_line67 = inv(Z_line67);
Y_line78 = inv(Z_line78);
Y_line89 = inv(Z_line89);

% transformer impedances, converted to system bases in pu
Z_trans_mccourtney = (Z_trans_0)*((480/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_trans_galvin = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3)); % Y grounded - Y grounded
Z_trans_hanks = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(500 * 1*10^3));
Z_trans_corbett = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_trans_jacc = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_trans_oneill = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_trans_jacc_south = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(100 * 1*10^3));
Z_trans_compton = (Z_trans_0)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3)); % Y grounded - Y grounded
% delta-connected capacitor impedance
S_cap = 400 * 10^3;  % VAR
Z_capacitor = (-1j)*((V_base_ll^2)/(S_cap));
% building impedances, converted to system bases in pu
Z_build_mccourtney = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_build_galvin = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_build_hanks = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(500 * 1*10^3));
Z_build_corbett = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_build_jacc_arena = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_build_oneill = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));
Z_build_jacc_south = (Z_build)*(a2^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(100 * 1*10^3));
Z_build_compton = (Z_build)*(a^2)*((V_base_ll/V_base_ll)^2)*((S_base)/(1000 * 1*10^3));

% converting to admittances
% all other transformers and capacitor are delta-connected, thus the zero
% sequence current doesn't flow to ground here and these can be left out of
% the calculation for the admittance matrix. Only the transformers and
% buildings at Galvin and Compton are included
Y_galvin = inv(Z_trans_galvin + Z_build_galvin);
Y_compton = inv(Z_trans_compton + Z_build_compton);

% building zero-impedance matrix
y11 = Y_line12 + Y_0;
y12 = -1*Y_line12;
y13 = 0;
y14 = 0;
y15 = 0;
y16 = 0;
y17 = 0;
y18 = 0;
y19 = 0;
y21 = y12;
y22 = Y_line12 + Y_line23;
y23 = -1*Y_line23;
y24 = 0;
y25 = 0;
y26 = 0;
y27 = 0;
y28 = 0;
y29 = 0;
y31 = y13;
y32 = y23;
y33 = Y_line23 + Y_line34 + Y_galvin;
y34 = -1*Y_line34;
y35 = 0;
y36 = 0;
y37 = 0;
y38 = 0;
y39 = 0;
y41 = y14;
y42 = y24;
y43 = y34;
y44 = Y_line34 + Y_line45 + Y_line46;
y45 = -1*Y_line45;
y46 = -1*Y_line46;
y47 = 0;
y48 = 0;
y49 = 0;
y51 = y15;
y52 = y25;
y53 = y35;
y54 = y45;
y55 = Y_line45;
y56 = 0;
y57 = 0;
y58 = 0;
y59 = 0;
y61 = y16;
y62 = y26;
y63 = y36;
y64 = y46;
y65 = y56;
y66 = Y_line46 + Y_line67;
y67 = -1*Y_line67;
y68 = 0;
y69 = 0;
y71 = y17;
y72 = y27;
y73 = y37;
y74 = y47;
y75 = y57;
y76 = y67;
y77 = Y_line67 + Y_line78; 
y78 = -1*Y_line78;
y79 = 0;
y81 = y18;
y82 = y28;
y83 = y38;
y84 = y48;
y85 = y58;
y86 = y68;
y87 = y78;
y88 = Y_line78 + Y_line89;
y89 = -1*Y_line89;
y91 = y19;
y92 = y29;
y93 = y39;
y94 = y49;
y95 = y59;
y96 = y69;
y97 = y79;
y98 = y89;
y99 = Y_line89 + Y_compton;

% put zero-sequence admittance matrix together
Y_zero = [y11 y12 y13 y14 y15 y16 y17 y18 y19;
          y21 y22 y23 y24 y25 y26 y27 y28 y29;
          y31 y32 y33 y34 y35 y36 y37 y38 y39;
          y41 y42 y43 y44 y45 y46 y47 y48 y49;
          y51 y52 y53 y54 y55 y56 y57 y58 y59;
          y61 y62 y63 y64 y65 y66 y67 y68 y69;
          y71 y72 y73 y74 y75 y76 y77 y78 y79;
          y81 y82 y83 y84 y85 y86 y87 y88 y89;
          y91 y92 y93 y94 y95 y96 y97 y98 y99];

% invert to get zero-sequence impedance matrix
Z_zero = inv(Y_zero);

% per-unit zer-sequence impedance matrix
Z_zero_pu = Z_zero / Z_base;

% per-unit zero-sequence admittance matrix
Y_zero_pu = Y_zero * Z_base;

% display results
disp('Zero-sequence impedance matrix for system: Z_zero = ');
disp(num2str(Z_zero));
disp(' ');
disp('Zero-sequence admittance matrix for system: Y_zero = ');
disp(num2str(Y_zero));
disp(' ');
disp('Zero-sequence per-unit impedance matrix for system: Z_zero_pu = ');
disp(num2str(Z_zero_pu));
disp(' ');
disp('Zero-sequence per-unit admittance matrix for system: Y_zero_pu = ');
disp(num2str(Y_zero_pu));
disp(' ');
disp(' ');
%% part (b)
disp('part (b): ');
% examining the case of a double line-to-ground fault b/t B and C phases
% at Corbett hall (fault occurring on bus 6) ... find the A,B,C voltages at
% bus 6 and find the fault current (in all phases) to ground at the fault

% assuming all buses begin at a "flat start": 1.0<0 pu
Vf = 1;

% non-symmetric fault value parameter definitions
% 'a' constants
a = -(1/2) + 1j*((sqrt(3))/(2));    % 1<120
% 'A' matrix
A = [1 1 1;
     1 a^2 a;
     1 a a^2];
% 'Ah'
Ah = [1 1 1;
      1 a a^2;
      1 a^2 a];

% for a double line-to-ground fault of B and C phases, know that:
    % Ia = 0, Vb = Vc = 0
    % Va0 = Va1 = Va2 = VA/3
% using equivalent circuit for double line-to-ground (from 5/3 class notes)
Ia1 = Vf / (Z_pos_pu(6,6) + ((Z_neg_pu(6,6) * Z_zero_pu(6,6))/(Z_neg_pu(6,6) + Z_zero_pu(6,6)))); % eqn 13-69
Va1 = Vf - (Ia1)*(Z_pos_pu(6,6));
Va0 = Va1; 
Va2 = Va1;
Va012 = [Va0; Va1; Va2];
Ia2 = -1*((Va2)/(Z_neg_pu(6,6)));
Ia0 = -1*((Va0)/(Z_zero_pu(6,6)));
Ia012 = [Ia0; Ia1; Ia2];
% finding A,B,C phase of fault current
Iabc = A*Ia012;
Ia = Iabc(1);
Ib = Iabc(2);
Ic = Iabc(3);
disp('Fault Current (in all phases) to Ground:');
[mag,phase] = r2p2(Ia);
disp(['A-phase Fault Current at Bus 6: IF_A = ', num2str(mag), '<' num2str(phase), ' Apu']);
[mag,phase] = r2p2(Ib);
disp(['B-phase Fault Current at Bus 6: IF_B = ', num2str(mag), '<' num2str(phase), ' Apu']);
[mag,phase] = r2p2(Ic);
disp(['C-phase Fault Current at Bus 6: IF_C = ', num2str(mag), '<' num2str(phase), ' Apu']);
disp(' ');
% finding A,B,C voltages at Bus 6 post-fault
Vabc = A*Va012;
Va = Vabc(1);
Vb = Vabc(2);
Vc = Vabc(3);
disp('Per-unit A/B/C Voltages at Bus 6:');
[mag,phase] = r2p2(Va);
disp(['Pe-unit A-phase Voltage at Bus 6 at Fault: VA6pu = ', num2str(mag), '<' num2str(phase), ' Vpu']);
[mag,phase] = r2p2(Vb);
disp(['Per-unit B-phase Voltage at Bus 6 at Fault: VB6pu = ', num2str(mag), '<' num2str(phase), ' Vpu']);
[mag,phase] = r2p2(Vc);
disp(['Per-unit C-phase Voltage at Bus 6 at Fault: VC6pu = ', num2str(mag), '<' num2str(phase), ' Vpu']);
disp(' ');
disp('Real-valued A/B/C Voltages at Bus 6:');
[mag,phase] = r2p2((Va*V_base_ln));
disp(['Real-valued A-phase Voltage at Bus 6 at Fault: VA6 = ', num2str(mag), '<' num2str(phase), ' V']);
[mag,phase] = r2p2(Vb*V_base_ln);
disp(['Real-valued B-phase Voltage at Bus 6 at Fault: VB6 = ', num2str(mag), '<' num2str(phase), ' V']);
[mag,phase] = r2p2(Vc*V_base_ln);
disp(['Real-valued C-phase Voltage at Bus 6 at Fault: VC6 = ', num2str(mag), '<' num2str(phase), ' V']);
disp(' ');
disp(' ');

%% part (c)
disp('part (c): ');
% computing A,B,C voltages at all buses in the system after the fault
I0sys = [0; 0; 0; 0; 0; -1*Ia0; 0; 0; 0];
I1sys = [0; 0; 0; 0; 0; -1*Ia1; 0; 0; 0];
I2sys = [0; 0; 0; 0; 0; -1*Ia2; 0; 0; 0];

DelV0 = Z_zero_pu * I0sys;
DelV1 = Z_pos_pu * I1sys;
DelV2 = Z_neg_pu * I2sys;

disp('A/B/C Voltages At All Buses in System Post-Fault: ');
disp(' ');
num_buses = 9;
for i = 1:num_buses
    Delvec = [DelV0(i); DelV1(i); DelV2(i)];
    Orig = [0.0; Vf; 0.0];
    Symvec = Orig + Delvec;
    
    % save results for later
    if i == 6
        Va0_6 = Symvec(1);
        Va1_6 = Symvec(2);
        Va2_6 = Symvec(3);
    end
    
    if i == 7
        Va0_7 = Symvec(1);
        Va1_7 = Symvec(2);
        Va2_7 = Symvec(3);
    end
    
    % convert to A,B,C voltages with A-matrix
    ABC = A*Symvec;
    
    % print out results
    disp(['Bus ', num2str(i), ' Voltages:']);
    [mag,phase] = r2p2(ABC(1));
    disp(['Bus ', num2str(i), ' Per-unit A-phase Voltage: VA', num2str(i), 'pu = ', num2str(mag), '<' num2str(phase), ' Vpu']);
    [mag,phase] = r2p2(ABC(2));
    disp(['Bus ', num2str(i), ' Per-unit B-phase Voltage: VB', num2str(i), 'pu = ', num2str(mag), '<' num2str(phase), ' Vpu']);
    [mag,phase] = r2p2(ABC(3));
    disp(['Bus ', num2str(i), ' Per-unit C-phase Voltage: VC', num2str(i), 'pu = ', num2str(mag), '<' num2str(phase), ' Vpu']);
    [mag,phase] = r2p2(ABC(1)*(V_base_ln));
    disp(['Bus ', num2str(i), ' Real-valued A-phase Voltage: VA', num2str(i), ' = ', num2str(mag), '<' num2str(phase), ' V']);
    [mag,phase] = r2p2(ABC(2)*(V_base_ln));
    disp(['Bus ', num2str(i), ' Real-valued B-phase Voltage: VB', num2str(i), ' = ', num2str(mag), '<' num2str(phase), ' V']);
    [mag,phase] = r2p2(ABC(3)*(V_base_ln));
    disp(['Bus ', num2str(i), ' Real-valued C-phase Voltage: VC', num2str(i), ' = ', num2str(mag), '<' num2str(phase), ' V']);
    disp(' ');
    
end

% also, computing the A,B,C currents between buses 6 and 7
Ia0_6to7 = (Va0_6 - Va0_7)*(-1*Y_zero_pu(6,7));
Ia1_6to7 = (Va1_6 - Va1_7)*(-1*Y_pos_pu(6,7));
Ia2_6to7 = (Va2_6 - Va2_7)*(-1*Y_neg_pu(6,7));
Ia012_6to7 = [Ia0_6to7; Ia1_6to7; Ia2_6to7];
Iabc_6to7 = A * Ia012_6to7;
Ia_6to7 = Iabc_6to7(1);
Ib_6to7 = Iabc_6to7(2);
Ic_6to7 = Iabc_6to7(3);

% display results
disp(' ');
disp('Per-unit Current from Bus 6 to Bus 7 (in all phases): ');
[mag,phase] = r2p2(Iabc_6to7(1));
disp(['A-phase Current from Bus 6 to Bus 7: IA_6to7pu = ', num2str(mag), '<' num2str(phase), ' Apu']);
[mag,phase] = r2p2(Iabc_6to7(2));
disp(['B-phase Current from Bus 6 to Bus 7: IB_6to7pu = ', num2str(mag), '<' num2str(phase), ' Apu']);
[mag,phase] = r2p2(Iabc_6to7(3));
disp(['C-phase Current from Bus 6 to Bus 7: IC_6to7pu = ', num2str(mag), '<' num2str(phase), ' Apu']);
disp(' ');
disp('Real-valued Current from Bus 6 to Bus 7 (in all phases): ');
[mag,phase] = r2p2(Iabc_6to7(1) * I_base);
disp(['Real-valued A-phase Current from Bus 6 to Bus 7: IA_6to7 = ', num2str(mag), '<' num2str(phase), ' A']);
[mag,phase] = r2p2(Iabc_6to7(2) * I_base);
disp(['Real-valued B-phase Current from Bus 6 to Bus 7: IB_6to7 = ', num2str(mag), '<' num2str(phase), ' A']);
[mag,phase] = r2p2(Iabc_6to7(3) * I_base);
disp(['Real-valued C-phase Current from Bus 6 to Bus 7: IC_6to7 = ', num2str(mag), '<' num2str(phase), ' A']);
disp(' ');

%% end functions
% modified open-source functions for converting rectangular to polar
% r2p is for debugging and displaying rectangular quantities in polar
function [] = r2p(z)
% z = a + ib where a is real part & b is imaginary part
% magnitude = sqrt(a^2+b^2)
% angle = inver_tan(b/a)
a = real(z);
b = imag(z);
magnitude = sqrt(a^2 + b^2);
if magnitude <= 1e-10 
    magnitude = 0;
    a = 0;
end
if a > 0 && b > 0
    angle = (atan(abs(b/a)))*180/pi;
elseif a < 0 && b > 0
    angle = 180-((atan(abs(b/a)))*180/pi);
elseif a < 0 && b < 0
    angle = 180 + ((atan(abs(b/a)))*180/pi);
elseif a > 0 && b < 0
    angle =- ((atan(abs(b/a)))*180/pi);
elseif a == 0
    angle = 90;
end
disp([num2str(magnitude), '<', num2str(angle), + newline]);
end

% r2p2 is for returning the angle and magnitude for calculation purposes
% and for formatting the output
function [magnitude, angle] = r2p2(z)
% z = a + ib where a is real part & b is imaginary part
% magnitude = sqrt(a^2+b^2)
% angle = inver_tan(b/a)
a = real(z);
b = imag(z);
magnitude = sqrt(a^2 + b^2);
if magnitude <= 1e-10
    magnitude = 0;
    a = 0;
end
if a > 0 && b > 0
    angle = (atan(abs(b/a)))*180/pi;
elseif a < 0 && b > 0
    angle = 180-((atan(abs(b/a)))*180/pi);
elseif a < 0 && b < 0
    angle = 180 + ((atan(abs(b/a)))*180/pi);
elseif a > 0 && b < 0
    angle =- ((atan(abs(b/a)))*180/pi);
elseif a == 0
    angle = 90;
end
end
